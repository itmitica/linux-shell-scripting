# linux-shell-scripting


## shebang


the “#!” combo, the shebang, is used by the shell to decide which interpreter to run the rest of the script, and ignored by the shell that actually runs the script


`hello.sh`


```shell
#!/bin/sh
echo Hello World

```


## executable


to make a file executable in Linux, the executable mode bit needs to be enabled


```shell
$ chmod 755 ./hello.sh
$ ./hello.sh
Hello World

```


## parsing

the shell parses the arguments *BEFORE* passing them on to the program being called


`hello.sh`


```shell
#!/bin/sh

# echo takes any number of arguments

# call echo with *TWO* arguments
echo Hello World

# call echo with *ONE* argument
echo "Hello World"

```


## variables


### (not) quoting literals


`message=Hello World`: the shell will try to execute the command World after assigning `message=Hello`


### (not) quoting variables aka whitespaced variable values


`echo $message`: `echo` receives *TWO* arguments: `Hello` and `World`

`echo "$message"`: `echo` receives *ONE* argument: `"Hello World"`


#### curly braces

`echo "${messages}"`: `echo` receives *ONE* (innexisten, null) argument

`echo "${message}s"`: `echo` receives *TWO* arguments: `"Hello World"` and `"s"`


### whitespace around operators


`message="Hello World"` is variable assignement.

`message = "Hello World"` is program call: `message` with two parameters: `=` and `"Hello World"`


## operators


### spaces around


`if SPACE [ SPACE "$foo" SPACE = SPACE "bar" SPACE ]`: put spaces around all your operators


### [ aka test


`[` is actually a program, just like `ls`, so it must be surrounded by spaces


```shell
$ which [
/usr/bin/[

```

### 